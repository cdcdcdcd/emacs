;; -*- coding: utf-8; lexical-binding: t; -*-
;; sample use of abbrev


(define-skeleton hello-skeleton
  "Write a greeting"
  "Type name of user: "
  "hello, " str "!")

(define-skeleton my-org "Docstring." nil "[fn:: " _ "]\n\n")

(define-skeleton java-sysout "Docstring." nil "System.out.println(" _ ");")
(define-skeleton java-fori "Docstring." nil "for (int i = 0; i < " _ "; i++) {\n₠\n}")
(define-skeleton java-forj "Docstring." nil "for (int j = 0; j < " _ "; j++) {\n₠\n}")


(define-skeleton sc-synthdef "Docstring." nil "(
SynthDef.new(\\" _ ", {
₠
}).add;
)")

(define-skeleton bracket "Docstring." nil "(" _ ")")
(define-skeleton curly "Docstring." nil "{" _ "}")

(define-skeleton c-print "Docstring." nil "printf(\"" _ "\\n\"₠);\n₠")
(define-skeleton c-if "Docstring." nil "if (" _ ") {\n₠\n}")
(define-skeleton c-elif "Docstring." nil "else if (" _ ") {\n₠\n}")
(define-skeleton c-else "Docstring." nil "else {\n" _ "\n}")
(define-skeleton c-fori "Docstring." nil "for (size_t i = 0; i < " _ "; i++) {\n₠\n}")

(define-skeleton c-forxy "Docstring." nil "for (size_t x = 0; x < " _ "; x++) {\n    for (int y = 0; y < ₠; y++) {\n₠\n0}\n}")

(define-skeleton c-forj "Docstring." nil "for (size_t j = 0; j < " _ "; j++) {\n₠\n}")
(define-skeleton c-for "Docstring." nil "for (" _ ") {\n₠\n}")
(define-skeleton c-while "Docstring." nil "while (" _ ") {\n₠\n}")
(define-skeleton c-function "Docstring." nil _ "( ₠ ) {\n₠\n}")
(define-skeleton c-main "Docstring." nil "#include <stdio.h>\n#include <stdlib.h>\n\nint main (int argc, char** argv) {\n" _ "\n    return 0;\n}")
(define-skeleton c-include "Docstring." nil "#include \"" _ ".h\"")
(define-skeleton c-typedefstruct "Docstring" nil "typedef struct {\n    ₠\n} " _ ";")

(define-skeleton python-print "Docstring." nil "print(\"" _ "\"₠)\n₠")
(define-skeleton python-if "Docstring." nil "if " _ ":\n₠")
(define-skeleton python-elif "Docstring." nil "elif " _ ":\n₠")
(define-skeleton python-else "Docstring." nil "else:\n" _ "}")
(define-skeleton python-fori "Docstring." nil "for i in range(" _ "):\n")
(define-skeleton python-for "Docstring." nil "for " _ " in ₠:\n")
(define-skeleton python-while "Docstring." nil "while " _ ":\n₠")
(define-skeleton python-function "Docstring." nil "def " _ "(₠):\n₠")
(define-skeleton python-main "Docstring." nil "def main():\n" _ "\n    return 0")

(clear-abbrev-table global-abbrev-table)

;; (define-abbrev-table 'global-abbrev-table
;;   '(
;;     ;; jump symbol
;;     ("js" "₠")
;;     ;; English word abbrev
;;     ("bc" "because" )
;;     ("bg" "background" )
;;     ("bt" "between" )

;;     ;; English abridged words
;;     ("cnt" "can't" )
;;     ("ddnt" "didn't" )
;;     ("dnt" "don't" )

;;     ;; phrase abbrev
;;     ("afaik" "as far as i know" )
;;     ("atm" "at the moment" )
;;     ("ty" "thank you" )
;;     ("btw" "by the way" )

;;     ;; computing
;;     ("cfg" "context-free grammar" )
;;     ("cs" "computer science" )

;;     ;; tech company
;;     ("gc" "Google Chrome" )
;;     ("macos" "macOS" )
;;     ("msw" "Microsoft Windows" )

;;     ;; programing
;;     ;; ("b1" "" bracket)
;;     ;; ("b2" "" curly)
;;     ("ipa" "IP address" )
;;     ("jvm" "Java Virtual Machine" )
;;     ("rsi" "Repetitive Strain Injury" )
;;     ("subdir" "subdirectory" )
;;     ("db" "database" )

;;     ("evp" "environment variable" )
;;     ("guip" "graphical user interface" )
;;     ("oopp" "object oriented programing" )
;;     ("osp" "operating system" )

;;     ;; programing
;;     ("utf8" "-*- coding: utf-8 -*-" )

;;     ;; regex
;;     ("azt" "\\([A-Za-z0-9]+\\)")
;;     ("brackett" "\\[\\([^]]+?\\)\\]")
;;     ("curlyt" "“\\([^”]+?\\)”")
;;     ("digitst" "\\([0-9]+\\)")
;;     ("datet" "\\([0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]\\)")
;;     ("strt" "\\([^\"]+?\\)")

;;     ;; unicode
;;     ("hr" "--------------------------------------------------" )
;;     ("bu" "•" )
;;     ("catface" "😸" )
;;     ("hearts" "♥💕💓💔💖💗💘💝💞💟💙💚💛💜" )
;;     ("ra" "→" )

;;     ;;
;;     ))

;; ;; define abbrev for specific major mode
;; ;; the first part of the name should be the value of the variable major-mode of that mode
;; ;; e.g. for go-mode, name should be go-mode-abbrev-table
;; (progn
;;   (when (boundp 'test-mode-abbrev-table)
;;     (clear-abbrev-table test-mode-abbrev-table))
;;   (define-abbrev-table 'test-mode-abbrev-table
;;     '(("go" "package main
;; import \"fmt\"
;; func main() {
;;         fmt.Println(\"3\")
;; }")
;;       ("p" "fmt.Printf(\"%v\\n\", hh▮)")
;;       ("pl" "fmt.Println(hh▮)")
;;       ("r" "return")
;;       ("st" "string")
;;       ("eq" "==")
;;       ("v" "var x = 3")
;;       ("df" "x := 3")
;;       ("c" "const x = 3")
;;       ("f" "func ff(x int) int {
;;     return nil
;; }")
;;       ("if" "if 4 { 3 }")
;;       ("ie" " if err != nil { panic(err) }")
;;       ("ei" "else if x > 0 { 3 }")
;;       ("else" "else { 3 }")
;;       ("for" "for i := 0; i < 4; i++ { i }")
;;       ("fr" "for k, v := range xxx {
;; ▮
;;     }
;; "))))

(progn
  (when (boundp 'python-ts-mode-abbrev-table)
    (clear-abbrev-table python-ts-mode-abbrev-table))
  (define-abbrev-table 'python-ts-mode-abbrev-table
    '(
      ;; ("p" "" python-print)
       ("ret" "return")
      ;; ("f" "" python-function)
      ("incl" "import")
      ("eq" "==")
      ("ifs" "" python-if)
      ("ei" "" python-elif)
      ("els" "" python-else)
      ("fore" "" python-for)
      ("fori" "" python-fori)
      ("wh" "" python-while)
      ("ma" "" python-main)
      )))

(progn
  (when (boundp 'c-ts-mode-abbrev-table)
    (clear-abbrev-table c-ts-mode-abbrev-table))
  (define-abbrev-table 'c-ts-mode-abbrev-table
    '(
      ("p" "" c-print)
      ("ret" "return")
      ;; ("f" "" c-function)
      ("incl" "" c-include)
      ("eq" "==")
      ("ifs" "" c-if)
      ("ei" "" c-elif)
      ("els" "" c-else)
      ("fore" "" c-for)
      ("fori" "" c-fori)
      ("forj" "" c-forj)
      ("forxy" "" c-forxy)
      ("wh" "" c-while)
      ("ma" "" c-main)
      ("ts" "" c-typedefstruct)
      )))

(progn
 (when (boundp 'java-ts-mode-abbrev-table)
   (clear-abbrev-table java-ts-mode-abbrev-table))
 (define-abbrev-table 'java-ts-mode-abbrev-table
   '(
     ("sysout" "" java-sysout)
     ("fori" "" java-fori)
     ("forj" "" java-forj)
     ("nAL" "new ArrayList<>();")
     )))

(progn
 (when (boundp 'sclang-mode-abbrev-table)
   (clear-abbrev-table sclang-mode-abbrev-table))
 (define-abbrev-table 'sclang-mode-abbrev-table
   '(
     ("SDf" "" sc-synthdef)
    )))



(set-default 'abbrev-mode t)


