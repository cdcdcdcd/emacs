;;-*-coding: utf-8;-*-
(define-abbrev-table 'c-mode-abbrev-table
  '(
    ("ei" "" c-elif :count 0)
    ("els" "" c-else :count 0)
    ("eq" "==" nil :count 0)
    ("fore" "" c-for :count 0)
    ("fori" "" c-fori :count 0)
    ("forj" "" c-forj :count 0)
    ("forxy" "" c-forxy :count 0)
    ("ifs" "" c-if :count 0)
    ("incl" "" c-include :count 0)
    ("ma" "" c-main :count 0)
    ("p" "" c-print :count 0)
    ("ret" "return" nil :count 0)
    ("ts" "" c-typedefstruct :count 0)
    ("wh" "" c-while :count 0)
   ))

(define-abbrev-table 'c-ts-mode-abbrev-table
  '(
    ("ei" "" c-elif :count 0)
    ("els" "" c-else :count 0)
    ("eq" "==" nil :count 0)
    ("fore" "" c-for :count 0)
    ("fori" "" c-fori :count 0)
    ("forj" "" c-forj :count 0)
    ("forxy" "" c-forxy :count 0)
    ("ifs" "" c-if :count 0)
    ("incl" "" c-include :count 0)
    ("ma" "" c-main :count 0)
    ("p" "" c-print :count 0)
    ("ret" "return" nil :count 0)
    ("ts" "" c-typedefstruct :count 0)
    ("wh" "" c-while :count 0)
   ))

(define-abbrev-table 'java-mode-abbrev-table
  '(
    ("fori" "" java-fori :count 0)
    ("forj" "" java-forj :count 0)
    ("nAL" "new ArrayList<>();" nil :count 0)
    ("sysout" "" java-sysout :count 0)
   ))

(define-abbrev-table 'java-ts-mode-abbrev-table
  '(
    ("fori" "" java-fori :count 0)
    ("forj" "" java-forj :count 0)
    ("nAL" "new ArrayList<>();" nil :count 0)
    ("sysout" "" java-sysout :count 0)
   ))

(define-abbrev-table 'python-mode-abbrev-table
  '(
    ("ei" "" python-elif :count 0)
    ("els" "" python-else :count 0)
    ("eq" "==" nil :count 0)
    ("fore" "" python-for :count 0)
    ("fori" "" python-fori :count 0)
    ("ifs" "" python-if :count 0)
    ("incl" "import" nil :count 0)
    ("ma" "" python-main :count 0)
    ("ret" "return" nil :count 0)
    ("wh" "" python-while :count 0)
   ))

(define-abbrev-table 'python-ts-mode-abbrev-table
  '(
    ("ei" "" python-elif :count 0)
    ("els" "" python-else :count 0)
    ("eq" "==" nil :count 0)
    ("fore" "" python-for :count 0)
    ("fori" "" python-fori :count 0)
    ("ifs" "" python-if :count 0)
    ("incl" "import" nil :count 0)
    ("ma" "" python-main :count 0)
    ("ret" "return" nil :count 0)
    ("wh" "" python-while :count 0)
   ))

(define-abbrev-table 'sclang-mode-abbrev-table
  '(
    ("SDf" "" sc-synthdef :count 0)
   ))

(define-abbrev-table 'test-mode-abbrev-table
  '(
    ("c" "const x = 3" nil :count 0)
    ("df" "x := 3" nil :count 0)
    ("ei" "else if x > 0 { 3 }" nil :count 0)
    ("else" "else { 3 }" nil :count 0)
    ("eq" "==" nil :count 0)
    ("f" "func ff(x int) int {
    return nil
}" nil :count 0)
    ("for" "for i := 0; i < 4; i++ { i }" nil :count 0)
    ("fr" "for k, v := range xxx {
▮
    }
" nil :count 0)
    ("go" "package main
import \"fmt\"
func main() {
        fmt.Println(\"3\")
}" nil :count 0)
    ("ie" " if err != nil { panic(err) }" nil :count 0)
    ("if" "if 4 { 3 }" nil :count 0)
    ("p" "fmt.Printf(\"%v\\n\", hh▮)" nil :count 0)
    ("pl" "fmt.Println(hh▮)" nil :count 0)
    ("r" "return" nil :count 0)
    ("st" "string" nil :count 0)
    ("v" "var x = 3" nil :count 0)
   ))

