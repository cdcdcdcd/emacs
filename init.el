
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '(
               ("melpa" . "https://melpa.org/packages/")
               ("org" . "https://orgmode.org/elpa")
               ("elpa" . "https://elpa.gnu.org/packages/")
               )
             )


(add-to-list 'auto-mode-alist '("\\.c\\'" . c-ts-mode))
(add-to-list 'auto-mode-alist '("\\.java\\'" . java-ts-mode))
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-ts-mode))


;;https://emacs.stackexchange.com/questions/53004/improving-isearch
(defun isearch-repeat-forward+ ()
  (interactive)
  (unless isearch-forward
    (goto-char isearch-other-end))
  (isearch-repeat-forward)
  (unless isearch-success
    (isearch-repeat-forward)))

(defun isearch-repeat-backward+ ()
  (interactive)
  (when (and isearch-forward isearch-other-end)
    (goto-char isearch-other-end))
  (isearch-repeat-backward)
  (unless isearch-success
    (isearch-repeat-backward)))



(package-initialize)

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq initial-buffer-choice "~/dox/notes/agenda.org")

(require 'recentf)
(require 'sclang)
(require 'magit)
(require 'magit-section)

(recentf-mode 1)
(setq recentf-max-menu-items 25)


(add-to-list 'load-path "~/.emacs.d/lisp/")

(require 'xah-fly-keys)
(require 'haskell-mode)
(require 'tidal)

(setq tidal-boot-script-path "~/.cabal/share/BootTidal.hs")

;; specify a layout
(xah-fly-keys-set-layout "qwertz-en")

;; possible values
;; adnw , azerty , azerty-be , beopy , bepo , carpalx-qfmlwy , carpalx-qgmlwb , carpalx-qgmlwy , colemak , colemak-dhm , colemak-dhm-angle , colemak-dhk , dvorak , koy , neo2 , norman , programer-dvorak , pt-nativo , qwerty , qwerty-abnt , qwerty-no (qwerty Norwegian) , qwertz , workman

(xah-fly-keys 1)


;; BOOTSTRAP USE-PACKAGE
(setq use-package-always-ensure t)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-when-compile (require 'use-package))


(use-package gruvbox-theme
             :config
             (load-theme 'gruvbox t))

(use-package vertico
             :config
             (vertico-mode))

(setq ring-bell-function 'ignore)

(defun xah-dired-sort ()
  "Sort dired dir listing in different ways.
Prompt for a choice.
URL `http://xahlee.info/emacs/emacs/dired_sort.html'
Version: 2018-12-23 2022-04-07"
  (interactive)
  (let (xsortBy xarg)
    (setq xsortBy (completing-read "Sort by:" '( "date" "size" "name" )))
    (cond
     ((equal xsortBy "name") (setq xarg "-Al "))
     ((equal xsortBy "date") (setq xarg "-Al -t"))
     ((equal xsortBy "size") (setq xarg "-Al -S"))
     ((equal xsortBy "dir") (setq xarg "-Al --group-directories-first"))
     (t (error "logic error 09535" )))
    (dired-sort-other xarg )))

(require 'dired )
(define-key dired-mode-map (kbd "s") 'xah-dired-sort)


;; tes
(defun tes-open-hk()
    (interactive)
    (find-file "~/.config/dxhd/dxhd.sh")
  )

(global-set-key (kbd "M-C-x") 'tes-open-hk)


(defun ext-of-first (arg)
(progn
(car (last (split-string (car (last arg)) "\\.")))
)
)

(defun open-by-ext-type (arg)
  (setq ext (ext-of-first arg))
  (let ((args nil)) ; Initialize args variable
    (cond
      ((or (string-equal ext "png")
           (string-equal ext "jpg")
           (string-equal ext "webp")
           (string-equal ext "jpeg")
           (string-equal ext "bmp")
           (string-equal ext "ttg"))
       (if (= (length arg) 1)
           (setq args (nconc (list "" nil "sxiv-rifle") arg))
         (setq args (nconc (list "" nil "nsxiv") arg)))
       (apply 'start-process args))
      ((or (string-equal ext "mp4")
           (string-equal ext "webm")
           (string-equal ext "mkv")
           (string-equal ext "mp3")
           (string-equal ext "avi")
           (string-equal ext "ogg")
           (string-equal ext "flac")
           (string-equal ext "wav"))
       (apply 'start-process (nconc (list "" nil "mpv") arg)))
      (t
       (dired-open-file)))))


(defun tes-open-by-ext-type ()
(interactive)
(if (string-equal major-mode "dired-mode")
(open-by-ext-type (dired-get-marked-files))
)
)

(defun my-org-mode-setup ()
  "Setup for Org mode."
  ;; (local-set-key (kbd "RET") 'org-open-at-point)
  ;; (define-key xah-fly-command-map (kbd "DEL") 'org-toggle-inline-images)
  (define-key xah-fly-command-map (kbd "C-RET") 'org-open-at-point)
  )

(add-hook 'org-mode-hook 'my-org-mode-setup)

 ;;make escape key do both activate command mode and cancel
 ;;from Egor Maltsev
 ;;Version 2023-04-08 2023-04-09

 ;;how it works:
 ;;1. <escape> set to xah-fly-keys-command-mode-activate in xah-fly-keys-shared-map
 ;;2. <escape> set to xah-fly-keys-escape in xah-fly-keys-command-map
 ;;3. catched tty ESC translates to <escape>

 ;;1. hack escape:

 (progn
   (defun xah-fly-keys-escape ()
     (interactive)
     (when (region-active-p)
       (deactivate-mark))
     (when (active-minibuffer-window)
       (abort-recursive-edit)))

   (define-key xah-fly-command-map (kbd "<escape>")     'xah-fly-keys-escape))

 (progn
   (defvar xah-fly-keys-fast-keyseq-timeout 200)

   (defun xah-fly-keys-tty-ESC-filter (map)
     (if (and (equal (this-single-command-keys) [?\e])
              (sit-for (/ xah-fly-keys-fast-keyseq-timeout 1000.0)))
         [escape] map))

   (defun xah-fly-keys-lookup-key (map key)
     (catch 'found
       (map-keymap (lambda (k b) (if (equal key k) (throw 'found b))) map)))

   (defun xah-fly-keys-catch-tty-ESC ()
     "Setup key mappings of current terminal to turn a tty's ESC into
 `escape'."
     (when (memq (terminal-live-p (frame-terminal)) '(t pc))
       (let ((esc-binding (xah-fly-keys-lookup-key input-decode-map ?\e)))
         (define-key input-decode-map
           [?\e] `(menu-item "" ,esc-binding :filter xah-fly-keys-tty-ESC-filter)))))

   (xah-fly-keys-catch-tty-ESC)

   (define-key key-translation-map (kbd "ESC") (kbd "<escape>")))

(defun search-and-jump-to-symbol ()
  "Searches forward for the symbol ₠ and deletes it. If not found, searches backwards and deletes it if found. Then, enters xah-fly-insert-mode."
  (interactive)
  (let ((forward-pos (search-forward "₠" nil t))
        (backward-pos (search-backward "₠" nil t)))
    (if forward-pos
        (progn
          (goto-char forward-pos)
          (delete-char -1)
          (xah-fly-insert-mode-activate))
      (if backward-pos
          (progn
            (goto-char backward-pos)
            (delete-char 1)
            (xah-fly-insert-mode-activate))
        (message "Symbol  not found")))))

(defun search-delete-insert-restore ()
  "Searches forward for the symbol ₠ and deletes it. If not found, searches backwards and deletes it if found. Then, enters xah-fly-insert-mode and restores cursor position."
  (interactive)
  (let ((forward-pos (search-forward "₠" nil t))
        (backward-pos (search-backward "₠" nil t))
        (saved-pos (point)))
    (if forward-pos
        (progn
          (goto-char forward-pos)
          (delete-char 1)
          (xah-fly-command-mode-activate)
          (goto-char saved-pos)
          (insert "₠"))
      (if backward-pos
          (progn
            (goto-char backward-pos)
            (delete-char 1)
            (xah-fly-command-mode-activate)
            (goto-char saved-pos)
            (insert "₠"))
        (message "Symbol ₠ not found")))))

(defun create-and-open-empty-file ()
  "Create and open an empty file."
  (interactive)
  (let ((file-name (read-file-name "Enter file name: ")))
    (write-region "" nil file-name)
    (find-file file-name)))

;; broken	  ;
;; (defun open-folder-with-lf ()
  ;; "Open the current folder in Dired with the 'lf' program."
  ;; (interactive)
  ;; (let ((folder (dired-current-directory)))
    ;; (if (file-directory-p folder)
        ;; (start-process "lf" nil "st" folder)
      ;; (message "Not a directory: %s" folder))))

;; Bind the function to a key for easy access



  (define-key xah-fly-leader-key-map (kbd "RET") 'bookmark-jump)
  (define-key xah-fly-leader-key-map (kbd "9") 'bookmark-set)
 (define-key xah-fly-leader-key-map  (kbd "v v") 'projectile-find-file)
 (define-key xah-fly-leader-key-map  (kbd "v f") 'find-file)
 (define-key xah-fly-leader-key-map  (kbd "v e") 'projectile-test-project)
 (define-key xah-fly-leader-key-map  (kbd "v c") 'projectile-compile-project)
 (define-key xah-fly-leader-key-map  (kbd "v g") 'projectile-grep-follow)
  (define-key xah-fly-leader-key-map (kbd "SPC") 'dired)
  (define-key xah-fly-leader-key-map (kbd "y a") 'search-anime-dir)
  (define-key xah-fly-leader-key-map (kbd ", a") 'compile)
  (define-key xah-fly-leader-key-map (kbd "y o") 'org-open-at-point)
  (define-key xah-fly-leader-key-map (kbd "y n") 'create-and-open-empty-file)
  (define-key xah-fly-leader-key-map (kbd "y m") 'revert-buffer-quick)

 (defun eol-dwim ()
   "Go to the end of the line, ignoring comments and trailing
 whitespace."
   (interactive)
   (let ((bol (line-beginning-position 1))
         (eol (line-end-position 1)))
     (if (condition-case nil
             (comment-search-forward eol)
           (error nil))
         (re-search-backward comment-start bol nil)
       (end-of-line 1))
     (skip-syntax-backward " " bol)))

 (defun insert-eol ()
   "Go eol then insert"
   (interactive)
   (progn
     (eol-dwim)
     (xah-fly-insert-mode-activate)
     )
   )

 (defun insert-bol ()
   "Go bol then insert"
   (interactive)
   (progn
     (beginning-of-line)
     (xah-fly-insert-mode-activate)
     )
   )

 (defun kill-line-insert ()
   "Kill the rest of the line then insert"
   (interactive)
   (progn
     (kill-line)
     (xah-fly-insert-mode-activate)
     )
   )

 (defun insert-above ()
   "new line below cursur"
   (interactive)
   (progn
     (beginning-of-line)
     (xah-fly-insert-mode-activate-newline)
     )
 )

 (defun replace-char ()
   "new line below cursur"
   (interactive)
   (progn
      (delete-char 1)
      (xah-fly-insert-mode-activate)
     )
 )

 ;; (global-set-key (kbd "A") 'insert-eol)
 ;; TODO only in commandmode

 (global-set-key (kbd "M-x") 'nil)

 (defun dired-toggle-ro-command-mode ()
   "toggle read only in dired and then command"
   (interactive)
   (progn
     (dired-toggle-read-only)
     (xah-fly-command-mode-activate)
     )
   )

 (defun update-cursor-color ()
   "Update cursor color based on the current major mode."
    (if (eq major-mode 'dired-mode)
    (progn
   (set-cursor-color "#FF0000")
   (setq cursor-type 'box)
   (xah-fly-insert-mode-activate)
   )
      (set-cursor-color "#FFFFFF")
      )
 )

 (add-hook 'post-command-hook 'update-cursor-color)

 (defun cancel-rename ()
   "cancel renaming process in dired"
   (interactive)
   (progn
     (wdired-abort-changes)
     (xah-fly-insert-mode-activate)
     )
   )

 (defun commit-rename ()
   "cancel commit rename in dired"
   (interactive)
   (progn
     (wdired-finish-edit)
     (xah-fly-insert-mode-activate)
     )
   )

 (defun dired-open-file ()
   "open file in dired"
   (interactive)
   (progn
     (dired-find-file)
     (xah-fly-command-mode-activate)
     )
   )


 (defun projectile-grep-follow()
        (interactive)
        (projectile-grep)
        (other-window 1))

 (require 'multiple-cursors)


 (xah-fly--define-keys
  xah-fly-command-map
  '(
    ("T" . xah-select-line)
    ("C" . kill-line-insert)
    ("O" . insert-above)
    (":" . replace-regexp)
    ("R" . replace-char)
    ("A" . insert-eol)
    ("F" . insert-bol)
    ("B" . dired-toggle-read-only)
    ("W" . commit-rename)
    ("K" . mc/mmlte--down)
    ("L" . mc/mark-next-like-this-word)
    ("I" . mc/mmlte--up)   
    ("Q" . cancel-rename)
    ("`" . universal-argument)
    )
  )


 (global-set-key (kbd "C-r") 'undo-redo)

 (blink-cursor-mode 0)
 (global-display-line-numbers-mode t)
 (setq display-line-numbers-type 'relative)

(defun dired-find-file-command-mode ()
"open file then go into command mode"
(interactive)
  (progn
    (dired-find-file)
    (xah-fly-command-mode-activate)
    )
  )

;; (define-key xah-fly-command-map  (kbd "SPC f f") 'projectile-find-file)
;; (define-key xah-fly-command-map  (kbd "SPC f g") 'projectile-grep-follow)
(defun tes-dired-execute-ddd ()
  "Execute the 'ddd' command with the current file in Dired as an argument."
  (interactive)
  (let ((file (dired-get-file-for-visit)))
    (if file
        (async-shell-command (format "dragon-drag-and-drop %s" (shell-quote-argument file)))
      (message "No file to execute 'ddd' on in Dired."))))




(define-key dired-mode-map  (kbd "SPC") nil)
(define-key dired-mode-map  (kbd "SPC h") 'beginning-of-buffer)
(define-key dired-mode-map  (kbd "SPC n") 'end-of-buffer)
(define-key dired-mode-map  (kbd "SPC SPC") 'dired)
(define-key dired-mode-map  (kbd "SPC RET") 'bookmark-jump)
(define-key dired-mode-map  (kbd "SPC i u") 'xah-open-in-terminal)
(define-key dired-mode-map  (kbd "SPC j j") 'describe-function)
(define-key dired-mode-map  (kbd "SPC j v") 'describe-key)
(define-key dired-mode-map  (kbd "SPC f") 'switch-to-buffer)
(define-key dired-mode-map  (kbd "t") 'dired-hide-details-mode)
(define-key dired-mode-map  (kbd "o") 'tes-dired-execute-ddd)
;; (define-key dired-mode-map  (kbd "SPC l f") 'open-folder-with-lf)

(define-key dired-mode-map (kbd "B") 'dired-toggle-ro-command-mode)
(define-key dired-mode-map (kbd "k") 'dired-next-line)
(define-key dired-mode-map (kbd "i") 'dired-previous-line)
(define-key dired-mode-map (kbd "j") 'dired-up-directory)
(define-key dired-mode-map (kbd "l") 'tes-open-by-ext-type)
(define-key dired-mode-map (kbd ",") 'xah-next-window-or-frame)
(define-key dired-mode-map (kbd "RET") 'dired-find-file-command-mode)
(define-key dired-mode-map (kbd "`") 'universal-argument)
(global-set-key (kbd "<f8>") 'eval-region)
(define-key global-map (kbd "<f2>") 'mode-line-other-buffer)
;; 
;; (define-key dired-mode-map (kbd "o") 'tes-open-by-ext-type)



 ;; (define-key dired-mode-map (kbd "SPC") 'dired)
 ;; (define-key dired-mode-map (kbd "RET") 'bookmark-jump)

 (global-set-key (kbd "<f10>") 'tab-undo)
 (global-set-key (kbd "<f11>") 'tab-close)
 (global-set-key (kbd "<f9>") 'tab-new)
 (global-set-key (kbd "<f3>") 'tab-previous)
 (global-set-key (kbd "<f4>") 'tab-next)
 (global-set-key (kbd "<f5>") 'kmacro-start-macro-or-insert-counter)
 (global-set-key (kbd "<f6>") 'kmacro-end-or-call-macro)

 (defun toggle-dired-mark ()
   "Toggle marking/unmarking of the file under the cursor in Dired mode."
   (interactive)
   (save-excursion
     (beginning-of-line)
     (if (eq (char-after) ?\040) ; Check if the file is unmarked (space character)
         (dired-mark 1)            ; Mark the file if it's unmarked
       (dired-unmark 1)
       ))
   (dired-next-line 1)
   )         ; Unmark the file if it's marked

 (define-key dired-mode-map (kbd "m") 'toggle-dired-mark)
 (define-key dired-mode-map (kbd "v") 'dired-toggle-marks)
 (define-key dired-mode-map (kbd "a") 'execute-extended-command)
 (define-key dired-mode-map (kbd "n") 'isearch-forward)
 (define-key dired-mode-map (kbd "3") 'delete-other-windows)
 (define-key dired-mode-map (kbd "9") 'bookmark-set)
 (define-key dired-mode-map (kbd "u") 'dired-unmark-all-marks)
 (define-key dired-mode-map (kbd "RET") 'dired-open-file)

 ;; (global-set-key (kbd "C->") 'mc/mark-next-like-this)
 ;; (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
 ;; (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
 
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("d80952c58cf1b06d936b1392c38230b74ae1a2a6729594770762dc0779ac66b7" "2ff9ac386eac4dffd77a33e93b0c8236bb376c5a5df62e36d4bfa821d56e4e20" "72ed8b6bffe0bfa8d097810649fd57d2b598deef47c992920aef8b5d9599eefe" "19a2c0b92a6aa1580f1be2deb7b8a8e3a4857b6c6ccf522d00547878837267e7" default))
 '(package-selected-packages
   '(company magit-section magit haskell-mode vertico use-package undo-fu projectile multiple-cursors gruvbox-theme evil)))
 
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

 (defun search-anime-dir ()
   "find the anime directory"
   (interactive)
   (dired "~/*/anime/*")
  )


;; test
(defun move-cursor-down-n-times (n)
  "Move the cursor down N times."
  (interactive "nEnter the number of lines to move down: ")
  (dotimes (_ n)
    (next-line)))

(defun copy-line-to-end-and-return ()
  "Copy text from current position to end of line, then return to original position."
  (interactive)
  (let ((start (point)))
    (end-of-line)
    (kill-ring-save start (point))
    (goto-char start)))

(define-key xah-fly-leader-key-map  (kbd "y c") 'copy-line-to-end-and-return)
(define-key xah-fly-leader-key-map  (kbd "y r") 'recentf)


(define-derived-mode test-mode fundamental-mode "test"
  "major mode test"
  )

;; (define-derived-mode python-mode fundamental-mode "python"
  ;; "major mode for python"
  ;; )

(add-to-list 'auto-mode-alist '("\\.test\\'" . test-mode))
;; (add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))


(global-set-key (kbd "ſ") 'search-and-jump-to-symbol)


(use-package hippie-exp
  :bind ([remap dabbrev-expand] . hippie-expand)
  :commands (hippie-expand)
  :config
  (setq hippie-expand-try-functions-list
	'(
	  try-complete-file-name-partially
	  try-complete-file-name
	  try-expand-dabbrev
	  try-expand-dabbrev-all-buffers
	  try-expand-dabbrev-from-kill
	  try-complete-lisp-symbol-partially
	  try-complete-lisp-symbol
	  try-expand-all-abbrevs
	  try-expand-list
	  try-expand-line)))

(global-set-key (kbd "€") 'hippie-expand)

;;TODO set sclang-eval-region-or-line  on C-RET
;; (define-key sclang-mode-keymap (kbd "C-RET") 'sclang-eval-region-or-line)
(global-set-key (kbd "ð") 'sclang-eval-region-or-line)
(global-set-key (kbd "ß") 'pop-global-mark)

;; TODO eval block

(load "~/.emacs.d/abbrev.el")


(setq-default indent-tabs-mode nil)

;; backup
(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-by-copying t)
;; (setq delete-old-versions t
  ;; kept-new-versions 6
  ;; kept-old-versions 2
  ;; version-control t)


(server-start)

(add-hook 'after-init-hook 'global-company-mode)
